from setuptools import find_packages, setup

setup(
    name='gitlab-tags',
    version='1.0.1',
    packages=find_packages(),
    url='https://gitlab.com/nat13ho/gitlab-tags.git',
    license='',
    author='natho',
    author_email='nat13ho@gmail.com',
    description=''
)
